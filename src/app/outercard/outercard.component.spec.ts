import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutercardComponent } from './outercard.component';

describe('OutercardComponent', () => {
  let component: OutercardComponent;
  let fixture: ComponentFixture<OutercardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutercardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutercardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
