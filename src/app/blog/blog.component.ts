import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  isEditable:boolean = false;
  isCategoryEditable:boolean = false;

  constructor() { }

  ngOnInit() {
  }

  editToggleButton(){
    this.isEditable = !this.isEditable;
  }
  categoryDropdown(){
    this.isCategoryEditable = !this.isCategoryEditable
  }
}
