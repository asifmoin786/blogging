import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ForgetpasswordComponent } from './home/forgetpassword/forgetpassword.component';
import { VerificationComponent } from './home/verification/verification.component';
import { ChangepasswordComponent } from './home/changepassword/changepassword.component';
import { WallComponent } from './wall/wall.component';
import { CardComponent } from './card/card.component';
import { CategoriesWidgetComponent } from './categories-widget/categories-widget.component';
import { TopPostComponent } from './top-post/top-post.component';
import { ProfileHeaderComponent } from './profile-header/profile-header.component';
import { HeaderComponent } from './header/header.component';
import { BlogComponent } from './blog/blog.component';
import { CommentComponent } from './comment/comment.component';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { CardcollectionComponent } from './wall/cardcollection/cardcollection.component';
import { AdminComponent } from './wall/admin/admin.component';
import { OutercardComponent } from './outercard/outercard.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ForgetpasswordComponent,
    VerificationComponent,
    ChangepasswordComponent,
    WallComponent,
    CardComponent,
    CategoriesWidgetComponent,
    TopPostComponent,
    ProfileHeaderComponent,
    HeaderComponent,
    BlogComponent,
    CommentComponent,
    CardcollectionComponent,
    AdminComponent,
    OutercardComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    FroalaEditorModule.forRoot(), FroalaViewModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
