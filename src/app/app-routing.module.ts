import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { ForgetpasswordComponent } from './home/forgetpassword/forgetpassword.component';
import { VerificationComponent } from './home/verification/verification.component';
import { ChangepasswordComponent } from './home/changepassword/changepassword.component';
import { WallComponent } from './wall/wall.component';
import { BlogComponent } from './blog/blog.component';
import { CardcollectionComponent } from './wall/cardcollection/cardcollection.component';
import { AdminComponent } from './wall/admin/admin.component';

const routes: Routes = [
  { path: 'cardcollection', component: CardcollectionComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'timeline', component: WallComponent },
  { path: 'login', component: HomeComponent },
  { path: 'forgetpassword', component: ForgetpasswordComponent },
  { path: 'verification', component: VerificationComponent },
  { path: 'changepassword', component: ChangepasswordComponent },
  { path: 'blog', component: BlogComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    HttpClientModule
  ],
  exports: [RouterModule],
  providers: [
]
})
export class AppRoutingModule { }
